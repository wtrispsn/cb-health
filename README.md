## Instructions

Pull the source code: `git clone https://gitlab.com/wtrispsn/cb-health && cd cb-health`

Install deps and run tests: `yarn && yarn test`
